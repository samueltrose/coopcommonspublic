from rest_framework.viewsets import ModelViewSet

from .serializers import EntrySerializer
from .models import Entry


class EntryViewSet(ModelViewSet):
    serializer_class = EntrySerializer
    queryset = Entry.objects.all()
