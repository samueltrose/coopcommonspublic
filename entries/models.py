from django.db import models

class Author(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = "authors"

class Keyword(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = "keywords"

class Discipline(models.Model):
    name = models.CharField(max_length=200)
    slug = models.SlugField()

    class Meta:
        verbose_name_plural = "disciplines"



class Entry(models.Model):
    class Meta:
        verbose_name_plural = "entries"
    title = models.TextField(null=False)
    source = models.TextField(null=False)
    publication = models.TextField(null=True)
    published_date = models.TextField(null=True)
    url = models.URLField(max_length=280,null=True)
    findings = models.TextField(null=True)
    sentence = models.TextField(null=True)
    paragraph = models.TextField(null=True)
    page = models.TextField(null=True)
    created_date = models.DateTimeField(auto_now_add=True)
    modified_date = models.DateTimeField(auto_now=True)
    authors = models.ManyToManyField(Author,  blank=True, related_name='authors')
    keywords = models.ManyToManyField(Keyword, blank=True, related_name='keywords')
    disciplines = models.ManyToManyField(Discipline, blank=True, related_name='disciplines')

