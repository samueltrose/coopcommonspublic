from rest_framework_json_api import serializers

from entries.models import Entry, Author, Discipline, Keyword

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = (
            'id',
            'name',
        )

class DisciplineSerializer(serializers.ModelSerializer):
    class Meta:
        model = Discipline
        fields = (
            'id',
            'name',
        )

class KeywordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Keyword
        fields = (
            'id',
            'name',
        )


class EntrySerializer(serializers.ModelSerializer):
    included_serializers = {
        'authors': AuthorSerializer,
        'keywords': KeywordSerializer,
        'disciplines': DisciplineSerializer,
    }
    class Meta:
        model = Entry
        fields = (
            'id',
            'title',
            'authors',
            'keywords',
            'disciplines',
            'source',
            'publication',
            'published_date',
            'url',
            'findings',
            'sentence',
            'paragraph',
            'page'
        )
    class JSONAPIMeta:
        included_resources = ['authors','keywords','disciplines']